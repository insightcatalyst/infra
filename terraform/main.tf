terraform {
  required_version = ">= 1.0.0, < 2.0.0"
  backend "s3" {
    bucket = "ebeh-dev-tf-state"
    key = "global/s3/terraform.tfstate"
    region = "eu-west-2"

    dynamodb_table = "dev-tf-locks"
    encrypt = true
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}



provider "aws" {
    region = var.region
}





data "aws_vpc" "default" {
    default = true
}

data "aws_subnets" "default" {
    filter {
        name = "vpc-id"
        values = [data.aws_vpc.default.id ]
    }
}

resource "aws_launch_configuration" "tf-example-config" {
    image_id = "ami-09d091f4a278a78e5"
    instance_type = "t2.micro"
    security_groups = [aws_security_group.instance.id]

    user_data = <<-EOF
        #!/bin/bash
        echo "Hello, World" > index.html
        nohup busybox httpd -f -p ${"var.server_port"} &
        EOF

        lifecycle {
            create_before_destroy = true
        }
}

resource "aws_autoscaling_group" "tf-example-asg" {
    launch_configuration = aws_launch_configuration.tf-example-config.name 
    vpc_zone_identifier = data.aws_subnets.default.ids

    target_group_arns = [aws_lb_target_group.asg.arn]
    health_check_type = "ELB"

    min_size = 2
    max_size = 3

    tag {
        key = "Name"
        value = "tf-asg-example"
        propagate_at_launch = true
    }
}


resource "aws_lb" "tf-example-lb" {
    name = "tf-example-asg"
    load_balancer_type = "application"
    subnets = data.aws_subnets.default.ids
    security_groups = [aws_security_group.alb.id]
}

resource "aws_lb_listener" "http" {
    load_balancer_arn = aws_lb.tf-example-lb.arn 
    port = 80
    protocol = "HTTP"

    default_action {
        type = "fixed-response"

        fixed_response {
            content_type = "text/plain"
            message_body = "404: page not found"
            status_code = 404
        }
    }
}

resource "aws_lb_listener_rule" "asg" {
    listener_arn = aws_lb_listener.http.arn
    priority = 100

    condition {
        path_pattern {
            values = ["*"]
        }
    }

    action {
        type = "forward"
        target_group_arn = aws_lb_target_group.asg.arn
    }
}

resource "random_pet" "name" {
  length = 2
}
resource "aws_lb_target_group" "asg" {
    name = "tf-example-asg-${random_pet.name.id}"
    port = var.server_port
    protocol = "HTTP"
    vpc_id = data.aws_vpc.default.id

      health_check {
        path = "/"
        protocol = "HTTP"
        matcher = "200"
        interval = 15
        timeout = 3
        healthy_threshold = 2
        unhealthy_threshold = 2
    }
     lifecycle {
    create_before_destroy = true
  }
}
    

resource "aws_security_group" "alb" {
    name = var.alb_security_group_name
    description = "Allow HTTP inbound traffic"

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}


resource "aws_security_group" "instance" {
    name = var.instance_security_group_name
    description = "Allow HTTP inbound traffic"

    ingress {
        from_port = var.server_port
        to_port = var.server_port
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}


